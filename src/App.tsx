import { Col, Row } from "antd";
import "antd/dist/antd.css";
import { FC, useEffect, useState } from "react";
import Feeds from "./components/Feeds";
import Header from "./components/Header";
import Paginator from "./components/Paginator";
import SideMenu from "./components/SideMenu";

import "./App.css";
import Events from "./components/Events";
import { constUsers } from "./itemTypes";

const MAIN_ITEMS_LIMIT = 5;

export type ItemTypes = "feeds" | "events";

export type UserFullInfo = {
  id: number;
  nickname: string;
  email: string;
  password: string;
  url?: string;
};

const MainPageContent: FC<{
  user: UserFullInfo | null;
  menuKey: string;
  page: number;
  mainItems: number;
  setMainItems: React.Dispatch<React.SetStateAction<number>>;
}> = ({ user, menuKey, page, mainItems, setMainItems }) => {
  if (menuKey === "feeds") {
    return (
      <Feeds
        user={user}
        setMainItems={setMainItems}
        limit={MAIN_ITEMS_LIMIT}
        page={page}
      />
    );
  } else if (menuKey === "events") {
    return (
      <Events
        user={user}
        setMainItems={setMainItems}
        limit={MAIN_ITEMS_LIMIT}
        page={page}
      />
    );
  }
  return <></>;
};

function App() {
  const [user, setUser] = useState<UserFullInfo | null>({
    id: constUsers[0].id,
    nickname: constUsers[0].name,
    email: constUsers[0].email,
    password: constUsers[0].password,
    url: constUsers[0].url,
  });

  const [selectedMenuKey, setSelectedMenuKey] = useState<ItemTypes>("feeds");
  const [page, setPage] = useState<number>(1);
  const [mainItems, setMainItems] = useState(0);

  useEffect(() => {
    console.log(mainItems);
  }, [mainItems]);

  return (
    <div className="App">
      <Header user={user} setUser={setUser} />
      <Row>
        <Col span={4}>
          <SideMenu menuKey={selectedMenuKey} setMenuKey={setSelectedMenuKey} />
        </Col>
        <Col span={20}>
          {
            <MainPageContent
              user={user}
              menuKey={selectedMenuKey}
              page={page}
              mainItems={mainItems}
              setMainItems={setMainItems}
            />
          }
        </Col>
      </Row>
      <Paginator
        page={page}
        setPage={setPage}
        pageSize={MAIN_ITEMS_LIMIT}
        total={mainItems}
      />
    </div>
  );
}

export default App;
