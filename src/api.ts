import axios from "axios";
import { EventForm, FeedForm } from "./components/Form";

const api = `https://tranquil-taiga-89333.herokuapp.com/`;

export const template = async () => {};

// --------------------------------------------- NEWS ---------------------------------------------

export const getAllFeeds = async () => {
  axios.post(api + "news/all").then((res) => {
    console.log(res);
  });

  //   axios({
  //     method: "post",
  //     url: api + "news/all",
  //   }).then((res) => {
  //     console.log(res);
  //   });

  // await fetch("https://tranquil-taiga-89333.herokuapp.com/news/all", {
  //   method: "POST",
  //   headers: {
  //     "Content-Type": "application/json;charset=utf-8",
  //   },
  // }).then((res) => {
  //   console.log(res);
  // });
};

export const createFeed = (authorId: number, feed: FeedForm) => {
  const { name, description } = feed;
  const data = JSON.stringify({ authorId, name, description });

  axios.post(api + "news/create", data).then((res) => {
    console.log(res);
  });
};

export const editFeed = (authorId: number, feedId: number, feed: FeedForm) => {
  const { name, description } = feed;
  const data = JSON.stringify({ authorId, id: feedId, name, description });

  axios.post(api + `news/update`, data).then((res) => {
    console.log(res);
  });
};

export const deleteFeed = (feedId: number) => {
  axios.post(api + `news/${feedId}/delete`, feedId).then((res) => {
    console.log(res);
  });
};

// --------------------------------------------- EVENTS ---------------------------------------------

export const getAllEvents = () => {
  axios.get(api + "events/all").then((res) => {
    console.log(res);
  });

  // fetch(api + "events/all").then((response) => {
  //   console.log(response);
  // });
};

export const createEvent = (authorId: number, event: EventForm) => {
  const { address, category, description, eventTime, link, title } = event;
  const data = JSON.stringify({
    authorId,
    address,
    category,
    description,
    eventTime,
    link,
    title,
  });

  axios.post(api + "events/create", data).then((res) => {
    console.log(res);
  });
};

export const editEvent = (
  authorId: number,
  event: EventForm & { feedId: number }
) => {
  const { address, category, description, eventTime, link, title } = event;
  const data = JSON.stringify({
    authorId,
    id: event.feedId,
    address,
    category,
    description,
    eventTime,
    link,
    title,
  });

  axios.post(api + "events/update", data).then((res) => {
    console.log(res);
  });
};

export const deleteEvent = (eventId: number) => {
  axios.post(api + `events/${eventId}/delete`, eventId).then((res) => {
    console.log(res);
  });
};

// --------------------------------------------- USERS ---------------------------------------------

export const createUser = (email: string, name: string) => {
  const data = JSON.stringify({ email, name });
  axios.post(api + `user/create`, data).then((res) => {
    console.log(res);
  });
};

// --------------------------------------------- COMMENTS NEWS ---------------------------------------------

export const getFeedComments = async (feedId: number) => {
  return await axios.post(api + `news/comment/${feedId}`).then((res) => {
    console.log(res);
    return res;
  });
};

export const createFeedComment = (
  authorId: number,
  feedId: number,
  text: string
) => {
  const data = JSON.stringify({
    newsId: feedId,
    text: text,
    userId: authorId,
  });

  axios.post(api + "news/comment/create", data).then((res) => {
    console.log(res);
  });
};

export const deleteFeedComment = (commentId: number) => {
  axios
    .post(api + `news/comment/${commentId}/delete`, commentId)
    .then((res) => {
      console.log(res);
    });
};

export const editFeedComment = (commentId: number, text: string) => {
  const data = JSON.stringify({ id: commentId, updatedText: text });

  axios.post(api + "news/comment/update", data).then((res) => {
    console.log(res);
  });
};

// --------------------------------------------- COMMENTS EVENTS   ---------------------------------------------

export const getEventComments = (eventId: number) => {
  axios.post(api + `events/comment/${eventId}`).then((res) => {
    console.log(res);
  });
};

export const createEventComment = (
  authorId: number,
  eventId: number,
  text: string
) => {
  const data = JSON.stringify({
    meetingId: eventId,
    authorId,
    text,
  });

  axios.post(api + "events/comment/create", data).then((res) => {
    console.log(res);
  });
};

export const deleteEventComment = (commentId: number) => {
  axios
    .post(api + `events/comment/${commentId}/delete`, commentId)
    .then((res) => {
      console.log(res);
    });
};

export const editEventComment = (commentId: number, text: string) => {
  const data = JSON.stringify({ commentId, text: text });

  axios.post(api + "events/comment/update", data).then((res) => {
    console.log(res);
  });
};
