import { Avatar, Card, Dropdown, Menu, Modal, Space, Typography } from "antd";
import {
  EditOutlined,
  DeleteOutlined,
  ExclamationCircleOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { FC, useState } from "react";
import { EventType, getDateEvent, getDateFeed } from "../../../itemTypes";

import "./index.css";
import { deleteEvent } from "../../../api";
import ViewSingle from "../../ViewSingle";
import EditEvent from "../../Form/Event/edit";
import { UserFullInfo } from "../../../App";
const { Text, Link } = Typography;

const DESCRIPTION_SHOW_MORE = 300;

function descriptionShow(description: string, isShown: boolean) {
  return isShown
    ? description
    : description.substring(0, DESCRIPTION_SHOW_MORE);
}

const OptionsButton: FC<{
  userId: number;
  event: EventType;
}> = ({ userId, event }) => {
  const [showEdit, setShowEdit] = useState(false);

  function confirm() {
    Modal.confirm({
      title: "Уверены, что хотите удалить мероприятие?",
      icon: <ExclamationCircleOutlined />,
      okText: "Удалить",
      cancelText: "Отмена",
      onOk: () => {
        deleteEvent(event.id);
      },
    });
  }

  const menu = (
    <Menu>
      <Menu.Item
        key="1"
        icon={<EditOutlined />}
        onClick={() => {
          setShowEdit(true);
        }}
      >
        Редактировать
      </Menu.Item>
      <Menu.Item key="2" icon={<DeleteOutlined />} onClick={confirm}>
        Удалить
      </Menu.Item>
    </Menu>
  );

  return (
    <>
      <Dropdown.Button
        overlay={menu}
        trigger={["click"]}
        style={{ fontSize: "22px" }}
      />
      <EditEvent
        userId={userId}
        visible={showEdit}
        setVisible={setShowEdit}
        event={event}
      />
    </>
  );
};

const SimpleEvent: FC<{ user: UserFullInfo | null; event: EventType }> = ({
  user,
  event,
}) => {
  const [showDescription] = useState(false);
  const [showView, setShowView] = useState(false);

  return (
    <div className="site-card-border-less-wrapper">
      <ViewSingle
        user={user}
        visible={showView}
        setVisible={setShowView}
        type="event"
        single={event}
      />
      <Card className="card" hoverable bordered={true}>
        <Space className="header" align="center" style={{ height: "32px" }}>
          <Space>
            <Avatar icon={<UserOutlined />} src={event.author.url ?? ""} />
            <Space direction="vertical" size={0}>
              <>{event.author.name}</>
              <h5>{getDateEvent(event.creationTime)}</h5>
            </Space>
          </Space>

          <Space align="center">
            {user?.id === event.author.id && (
              <OptionsButton userId={user.id} event={event}></OptionsButton>
            )}
          </Space>
        </Space>

        <Space size={0} direction="vertical" className="middleSpace">
          <h2
            onClick={() => {
              setShowView(true);
            }}
          >
            {event.name}
          </h2>
          <i>{event.category}</i>
        </Space>
        <Text
          style={{ width: "100%", display: "block" }}
          onClick={() => {
            setShowView(true);
          }}
        >
          {descriptionShow(event.description, showDescription)}
        </Text>
        {event.description.length >= DESCRIPTION_SHOW_MORE && (
          <Link
            onClick={() => {
                setShowView(true);
            }}
          >
              ...показать детали
          </Link>
        )}
      </Card>
    </div>
  );
};

export default SimpleEvent;
