import { FC, useEffect, useState } from "react";
import { getAllEvents } from "../../api";
import { UserFullInfo } from "../../App";
import { constEvents, EventType } from "../../itemTypes";
import SimpleEvent from "./SimpleEvent";

// import "./index.css";

type EventsProps = {
  user: UserFullInfo | null;
  limit: number;
  page: number;
  setMainItems: React.Dispatch<React.SetStateAction<number>>;
};

const Events: FC<EventsProps> = ({ user, limit, page, setMainItems }) => {
  // rest request
  // use *setFeeds* for save info for server

  useEffect(() => {
    getAllEvents();
  });

  const [events, setEvents] = useState<EventType[]>(constEvents);

  useEffect(() => {
    setMainItems(events.length);
  }, [events, setMainItems]);

  return (
    <div className="feeds">
      {events.slice((page - 1) * limit, page * limit).map((f) => (
        <SimpleEvent user={user} key={f.id} event={f} />
      ))}
    </div>
  );
};

export default Events;
