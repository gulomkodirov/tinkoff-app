import { Avatar, Card, Dropdown, Menu, Modal, Space, Typography } from "antd";
import {
  EditOutlined,
  DeleteOutlined,
  ExclamationCircleOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { FC, useState } from "react";
import { FeedType, getDateEvent } from "../../../itemTypes";

import "./index.css";
import { deleteFeed } from "../../../api";
import ViewSingle from "../../ViewSingle";
import EditFeed from "../../Form/Feed/edit";
import { UserFullInfo } from "../../../App";
const { Text, Link } = Typography;

const DESCRIPTION_SHOW_MORE = 300;

function descriptionShow(description: string, isShown: boolean) {
  return isShown
    ? description
    : description.substring(0, DESCRIPTION_SHOW_MORE);
}

const OptionsButton: FC<{
  userId: number;
  feed: FeedType;
}> = ({ userId, feed }) => {
  const [showEdit, setShowEdit] = useState(false);

  function confirm() {
    Modal.confirm({
      title: "Уверены, что хотите удалить новость?",
      icon: <ExclamationCircleOutlined />,
      okText: "Удалить",
      cancelText: "Отмена",
      onOk: () => {
        deleteFeed(feed.id);
      },
    });
  }

  const menu = (
    <Menu>
      <Menu.Item
        key="1"
        icon={<EditOutlined />}
        onClick={() => {
          setShowEdit(true);
        }}
      >
        Редактировать
      </Menu.Item>
      <Menu.Item key="2" icon={<DeleteOutlined />} onClick={confirm}>
        Удалить
      </Menu.Item>
    </Menu>
  );

  return (
    <>
      <Dropdown.Button
        overlay={menu}
        trigger={["click"]}
        style={{ fontSize: "22px" }}
      />
      <EditFeed
        userId={userId}
        visible={showEdit}
        setVisible={setShowEdit}
        feed={feed}
      />
    </>
  );
};

const SimpleFeed: FC<{ user: UserFullInfo | null; feed: FeedType }> = ({
  user,
  feed,
}) => {
  const [showDescription] = useState(false);
  const [showView, setShowView] = useState(false);

  return (
    <>
      <ViewSingle
        user={user}
        visible={showView}
        setVisible={setShowView}
        type="feed"
        single={feed}
      />
      <Card className="card" hoverable bordered={true}>
        <Space className="header" align="center" style={{ height: "32px" }}>
          <Space>
            <Avatar icon={<UserOutlined />} src={feed.author.url ?? ""} />
            <Space direction="vertical" size={0}>
              <>{feed.author.name}</>
              <h5>{getDateEvent(feed.publicationDate)}</h5>
            </Space>
          </Space>

          <Space align="center">
            {user?.id === feed.author.id && (
              <OptionsButton userId={user.id} feed={feed}></OptionsButton>
            )}
          </Space>
        </Space>

        <Space size={0} direction="vertical" className="middleSpace">
          <h2
            onClick={() => {
              setShowView(true);
            }}
          >
            {feed.title}
          </h2>
        </Space>
        <Text
          style={{ width: "100%", display: "block" }}
          onClick={() => {
            setShowView(true);
          }}
        >
          {descriptionShow(feed.description, showDescription)}
        </Text>
        {feed.description.length >= DESCRIPTION_SHOW_MORE && (
          <Link
            onClick={() => {
              setShowView(true);
            }}
          >
            ...показать детали
          </Link>
        )}
      </Card>
    </>
  );
};

export default SimpleFeed;
