import { FC, useEffect, useState } from "react";
import SimpleFeed from "./SimpleFeed";
import "./index.css";
import { constFeeds, FeedType } from "../../itemTypes";
import { getAllFeeds } from "../../api";
import { UserFullInfo } from "../../App";

type FeedsProps = {
  user: UserFullInfo | null;
  limit: number;
  page: number;
  setMainItems: React.Dispatch<React.SetStateAction<number>>;
};

const Feeds: FC<FeedsProps> = ({ user, limit, page, setMainItems }) => {
  // rest request
  // use *setFeeds* for save info for server

  const [feeds, setFeeds] = useState<FeedType[]>(constFeeds);

  useEffect(() => {
    getAllFeeds();
    // deleteFeed(3);
  }, []);

  useEffect(() => {
    setMainItems(feeds.length);
  }, [feeds, setMainItems]);

  return (
    <div className="feeds">
      {feeds.slice((page - 1) * limit, page * limit).map((f) => (
        <SimpleFeed user={user} key={f.id} feed={f} />
      ))}
    </div>
  );
};

export default Feeds;
