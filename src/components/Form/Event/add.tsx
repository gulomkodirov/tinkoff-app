import { DatePicker, Input, Select, Space } from "antd";
import { FC, useEffect, useState } from "react";
import { EventForm } from "..";
import { constCategories } from "../../../itemTypes";

const AddEvent: FC<{
  form: EventForm;
  setForm: React.Dispatch<React.SetStateAction<EventForm>>;
}> = ({ form, setForm }) => {
  const [date, setDate] = useState<Date>();

  useEffect(() => {
    if (date) {
      setForm({ ...form, eventTime: date });
    }
    // eslint-disable-next-line
  }, [date, setForm]);

  return (
    <Space direction="vertical" style={{ width: "100%" }}>
      <Input
        placeholder="Название"
        value={form.title}
        onChange={(e) => {
          setForm({ ...form, title: e.target.value });
        }}
      />
      <Select
        defaultValue="Другое"
        style={{ width: "100%" }}
        onChange={(value: string) => setForm({ ...form, category: value })}
      >
        {constCategories.map((cat) => (
          <Select.Option value={cat}>{cat}</Select.Option>
        ))}
      </Select>
      <DatePicker
        style={{ width: "100%" }}
        showTime
        placeholder="Выберите дату"
        onChange={(moment) => {
          if (moment) {
            setDate(new Date(moment.format()));
          }
        }}
      />
      <Input
        placeholder="Ссылка"
        value={form.link}
        onChange={(e) => {
          setForm({ ...form, link: e.target.value });
        }}
      />
      <Input
        placeholder="Адрес"
        value={form.address}
        onChange={(e) => {
          setForm({ ...form, address: e.target.value });
        }}
      />
      <Input.TextArea
        style={{ resize: "none" }}
        placeholder="Описание"
        rows={15}
        value={form.description}
        onChange={(e) => {
          setForm({ ...form, description: e.target.value });
        }}
      />
    </Space>
  );
};

export default AddEvent;
