import { DatePicker, Drawer, Input, Select, Space } from "antd";
import { FC, useState } from "react";
import { DrawerFooter } from "./..";
import { editEvent } from "../../../api";
import { constCategories, EventType } from "../../../itemTypes";

const EditEvent: FC<{
  userId: number;
  visible: boolean;
  setVisible: React.Dispatch<React.SetStateAction<boolean>>;
  event: EventType;
}> = ({ userId, visible, setVisible, event }) => {
  const [newTitle, setTitle] = useState(event.name);
  const [newDescription, setDescription] = useState(event.description);
  const [newCategory, setCategory] = useState(event.category);
  const [newDate, setDate] = useState(event.eventTime);
  const [newLink, setLink] = useState(event.link);
  const [newAddress, setAddress] = useState(event.address);

  return (
    <Drawer
      title="Редактирование мероприятия"
      placement="right"
      width={600}
      onClose={() => {
        setVisible(false);
      }}
      visible={visible}
      footer={
        <DrawerFooter
          type="edit"
          onOk={() => {
            editEvent(userId, {
              feedId: event.id,
              title: newTitle,
              description: newDescription,
              category: newCategory,
              eventTime: newDate,
              link: newLink,
              address: newAddress,
            });
          }}
          onCancel={() => {
            setVisible(false);
          }}
        />
      }
    >
      <Space direction="vertical" style={{ width: "100%" }}>
        <Input
          placeholder="Название"
          value={newTitle}
          onChange={(e) => {
            setTitle(e.target.value);
          }}
        />
        <Select
          defaultValue="Другое"
          style={{ width: "100%" }}
          onChange={(value: string) => setCategory(value)}
        >
          {constCategories.map((cat) => (
            <Select.Option value={cat}>{cat}</Select.Option>
          ))}
        </Select>
        <DatePicker
          style={{ width: "100%" }}
          showTime
          placeholder="Выберите дату"
          onChange={(moment) => {
            if (moment) {
              setDate(new Date(moment.format()));
            }
          }}
        />
        <Input
          placeholder="Ссылка"
          value={newLink}
          onChange={(e) => {
            setLink(e.target.value);
          }}
        />
        <Input
          placeholder="Адрес"
          value={newAddress}
          onChange={(e) => {
            setAddress(e.target.value);
          }}
        />
        <Input.TextArea
          style={{ resize: "none" }}
          placeholder="Описание"
          rows={15}
          value={newDescription}
          onChange={(e) => {
            setDescription(e.target.value);
          }}
        />
      </Space>
    </Drawer>
  );
};

export default EditEvent;
