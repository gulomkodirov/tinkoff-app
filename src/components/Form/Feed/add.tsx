import { Input, Space } from "antd";
import { FC } from "react";
import { FeedForm } from "..";
import "../index.css";

type AddFeedProps = {
  form: FeedForm;
  setForm: React.Dispatch<React.SetStateAction<FeedForm>>;
};

const AddFeed: FC<AddFeedProps> = ({ form, setForm }) => {
  return (
    <Space direction="vertical" style={{ width: "100%" }}>
      <Input
        className="formItem"
        placeholder="Название"
        value={form.name}
        onChange={(e) => {
          setForm({ ...form, name: e.target.value });
        }}
      />
      <Input.TextArea
        style={{ resize: "none" }}
        placeholder="Описание"
        rows={15}
        value={form.description}
        onChange={(e) => {
          setForm({ ...form, description: e.target.value });
        }}
      />
    </Space>
  );
};

export default AddFeed;
