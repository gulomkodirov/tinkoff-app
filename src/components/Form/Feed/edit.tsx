import { Drawer, Input, Space } from "antd";
import { FC, useState } from "react";
import { DrawerFooter } from "./..";
import { editFeed } from "../../../api";
import { FeedType } from "../../../itemTypes";

type EditFeedProps = {
  userId: number;
  visible: boolean;
  setVisible: React.Dispatch<React.SetStateAction<boolean>>;
  feed: FeedType;
};

const EditFeed: FC<EditFeedProps> = ({ userId, visible, setVisible, feed }) => {
  const [newTitle, setTitle] = useState(feed.title);
  const [newDescription, setDescription] = useState(feed.description);

  return (
    <Drawer
      title="Редактирование новости"
      placement="right"
      width={600}
      onClose={() => {
        setVisible(false);
      }}
      visible={visible}
      footer={
        <DrawerFooter
          type="edit"
          onOk={() => {
            editFeed(userId, feed.id, {
              name: newTitle,
              description: newDescription,
            });
          }}
          onCancel={() => {
            setVisible(false);
          }}
        />
      }
    >
      <Space direction="vertical" style={{ width: "100%" }}>
        <Input
          placeholder="Название"
          value={newTitle}
          onChange={(e) => {
            setTitle(e.target.value);
          }}
        />
        <Input.TextArea
          style={{ resize: "none" }}
          placeholder="Описание"
          rows={15}
          value={newDescription}
          onChange={(e) => {
            setDescription(e.target.value);
          }}
        />
      </Space>
    </Drawer>
  );
};

export default EditFeed;
