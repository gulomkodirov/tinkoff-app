import { Button, Drawer, Radio, Space } from "antd";
import { FC, useState } from "react";
import { PlusOutlined } from "@ant-design/icons";
import AddFeed from "./Feed/add";
import AddEvent from "./Event/add";
import { createEvent, createFeed } from "../../api";
import { ItemTypes } from "../../App";
import "./index.css";

const DEFAULT_DATE = new Date("2000-01-01T00:00:00");

type DrawerFooterProps = {
  type: "create" | "edit";
  onOk: () => void;
  onCancel: () => void;
};

export type FeedForm = {
  name: string;
  description: string;
};

export type EventForm = {
  title: string;
  description: string;
  category: string;
  eventTime: Date;
  link?: string;
  address?: string;
};

export const DrawerFooter: FC<DrawerFooterProps> = ({
  type,
  onOk,
  onCancel,
}) => {
  return (
    <div className="drawerFooter">
      <Space align="center">
        <Button type="primary" onClick={onOk}>
          {type === "create" ? "Создать" : "Сохранить"}
        </Button>
        <Button onClick={onCancel}>Отмена</Button>
      </Space>
    </div>
  );
};

const AddButton: FC<{ userId: number }> = ({ userId }) => {
  const [itemType, setItemType] = useState<ItemTypes>("feeds");

  const [visible, setVisible] = useState(false);

  const [formFeed, setFormFeed] = useState<FeedForm>({
    name: "",
    description: "",
  });

  const [formEvent, setFormEvent] = useState<EventForm>({
    title: "",
    description: "",
    category: "",
    eventTime: DEFAULT_DATE,
  });

  function saveFeed(): void {
    createFeed(userId, formFeed); // HERE userId
  }

  function saveEvent(): void {
    console.log(formEvent);
    createEvent(userId, formEvent); // HERE userId
  }

  function save(): void {
    if (itemType === "feeds") {
      console.log("feeds");

      saveFeed();
    }
    if (itemType === "events") {
      console.log("events");
      saveEvent();
    }
  }

  return (
    <>
      <PlusOutlined
        style={{ fontSize: "22px" }}
        onClick={() => {
          setVisible(true);
        }}
      />
      <Drawer
        title={
          itemType === "feeds" ? "Создание новости" : "Создание мероприятия"
        }
        placement="right"
        width={600}
        onClose={() => {
          setVisible(false);
        }}
        visible={visible}
        footer={
          <DrawerFooter
            type="create"
            onOk={save}
            onCancel={() => {
              setVisible(false);
            }}
          />
        }
      >
        <Space direction="vertical" style={{ width: "100%" }} size="large">
          <Radio.Group
            onChange={(e) => {
              setItemType(e.target.value);
            }}
            value={itemType}
          >
            <Radio value={"feeds"}>Новость</Radio>
            <Radio value={"events"}>Мероприятие</Radio>
          </Radio.Group>
          {itemType === "feeds" && (
            <AddFeed form={formFeed} setForm={setFormFeed} />
          )}
          {itemType === "events" && (
            <AddEvent form={formEvent} setForm={setFormEvent} />
          )}
        </Space>
      </Drawer>
    </>
  );
};

export default AddButton;
