import { Button, Input, Modal, Space } from "antd";
import { useState } from "react";
import { EyeTwoTone, EyeInvisibleOutlined } from "@ant-design/icons";
import "./index.css";

const LoginButton = () => {
  const [isModalVisible, setIsModalVisible] = useState(false);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  function validate() {
    return !email || password.length <= 3;
  }

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  return (
    <Space>
      <Button
        className="headerButton"
        type="primary"
        onClick={() => {
          setIsModalVisible(true);
        }}
      >
        Вход
      </Button>
      <Modal
        title="Вход"
        visible={isModalVisible}
        okText="Вход"
        cancelText="Отмена"
        onOk={handleOk}
        onCancel={handleCancel}
        okButtonProps={{
          disabled: validate(),
        }}
      >
        <Space direction="vertical" style={{ width: "100%" }}>
          <Input
            placeholder="Пример: email@tinkoff.ru"
            value={email}
            onChange={(e) => {
              setEmail(e.target.value);
            }}
          />
          <Input.Password
            placeholder="Введите пароль"
            iconRender={(visible) =>
              visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
            }
            value={password}
            onChange={(e) => {
              setPassword(e.target.value);
            }}
          />
        </Space>
      </Modal>
    </Space>
  );
};

export default LoginButton;
