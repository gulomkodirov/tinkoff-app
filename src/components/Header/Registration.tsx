import { Button, Input, Modal, Space } from "antd";
import { useState } from "react";
import "./index.css";

const RegButton = () => {
  const [isModalVisible, setIsModalVisible] = useState(false);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [repeatPassword, setRepeatPassword] = useState("");

  function validate() {
    return !email || password !== repeatPassword || password.length <= 3;
  }

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  return (
    <>
      <Button
        className="headerButtonLight"
        onClick={() => {
          setIsModalVisible(true);
        }}
      >
        Регистрация
      </Button>
      <Modal
        title="Регистрация"
        visible={isModalVisible}
        okText="Регистрация"
        cancelText="Отмена"
        onOk={handleOk}
        onCancel={handleCancel}
        okButtonProps={{
          disabled: validate(),
        }}
      >
        <Space direction="vertical" style={{ width: "100%" }}>
          <Input
            placeholder="Пример: email@tinkoff.ru"
            value={email}
            onChange={(e) => {
              setEmail(e.target.value);
            }}
          />
          <Input.Password
            placeholder="Введите пароль"
            value={password}
            onChange={(e) => {
              setPassword(e.target.value);
            }}
          />
          <Input.Password
            placeholder="Повторите пароль"
            value={repeatPassword}
            onChange={(e) => {
              setRepeatPassword(e.target.value);
            }}
          />
        </Space>
      </Modal>
    </>
  );
};

export default RegButton;
