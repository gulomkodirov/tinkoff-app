import { Avatar, Button, Popover, Space } from "antd";
import LoginButton from "./Login";
import RegButton from "./Registration";
import AddButton from "../Form";
import Search from "../Search";
import {
  UserOutlined,
  ExclamationCircleOutlined,
  EyeOutlined,
  EyeInvisibleOutlined,
} from "@ant-design/icons";
import { FC, useState } from "react";
import confirm from "antd/lib/modal/confirm";
import "./index.css";
import { UserFullInfo } from "../../App";

const UserInfo: FC<{ user: UserFullInfo | null }> = ({ user }) => {
  const [showPass, setShowPass] = useState(false);

  function showPassword(pass: string, isShown: boolean): string {
    return isShown ? pass : Array(pass.length).fill("*").join("");
  }

  return (
    user && (
      <div>
        <p>{user.nickname}</p>
        <p>email: {user.email}</p>
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <p>password: {showPassword(user.password, showPass)}</p>
          {!showPass && (
            <EyeOutlined
              onClick={() => {
                setShowPass(true);
              }}
            />
          )}
          {showPass && (
            <EyeInvisibleOutlined
              onClick={() => {
                setShowPass(false);
              }}
            />
          )}
        </div>
      </div>
    )
  );
};

const buttonsStyle = {
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
};

type HeaderProps = {
  user: UserFullInfo | null;
  setUser: React.Dispatch<React.SetStateAction<UserFullInfo | null>>;
};

const Header: FC<HeaderProps> = ({ user, setUser }) => {
  // const [formType, setFormType] = useState("feeds"); // or event

  const userLogout = () => {
    confirm({
      title: "Вы уверены, что хотите выйти?",
      icon: <ExclamationCircleOutlined style={{ color: "Goldenrod" }} />,
      okText: "Да",
      cancelText: "Нет",
      onOk() {
        setUser(null);
      },
      onCancel() {},
    });
  };

  return (
    <>
      <div className="headerHeader">
        <div className="headerTitle">Tinkoff Internal</div>
        <div className="headerMenu">
          <Space style={{ height: "50px" }} size={30}>
            <Search />
            {user && <AddButton userId={user.id} />}
            {user && (
              <Popover
                placement="bottom"
                content={<UserInfo user={user} />}
                trigger="click"
              >
                {!user.url ? (
                  <UserOutlined style={{ fontSize: "22px" }} />
                ) : (
                  <Avatar src={user.url} />
                )}
              </Popover>
            )}
            {user && (
              <Button className="headerButton" onClick={userLogout}>
                Выход
              </Button>
            )}
            {!user && (
              <Space style={buttonsStyle}>
                <LoginButton />
                <RegButton />
              </Space>
            )}
          </Space>
        </div>
      </div>
    </>
  );
};

export default Header;
