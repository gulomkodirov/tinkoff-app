import { Pagination } from "antd";
import { FC } from "react";
import "./index.css";

type PaginatorProps = {
  page: number;
  setPage: React.Dispatch<React.SetStateAction<number>>;
  total: number;
  pageSize: number;
};

const Paginator: FC<PaginatorProps> = ({ page, setPage, total, pageSize }) => {
  return (
    <div className="paginator">
      {total > pageSize && (
        <Pagination
          current={page}
          pageSize={pageSize}
          onChange={(p, pageSize) => {
            setPage(p);
          }}
          total={total}
        />
      )}
    </div>
  );
};

export default Paginator;
