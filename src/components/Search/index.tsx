import { useState } from "react";
import { SearchOutlined, CloseOutlined } from "@ant-design/icons";
import { Input, Select, Space } from "antd";

import "./index.css";

const Search = () => {
  const [isVisible, setIsVisible] = useState(false);

  const [searchType, setSearchType] = useState("feeds");

  return (
    <>
      {!isVisible && (
        <SearchOutlined
          style={{ fontSize: "22px" }}
          onClick={() => {
            setIsVisible(true);
          }}
        />
      )}
      {isVisible && (
        <Space>
          <Select
            defaultValue="feeds"
            style={{ width: 150 }}
            onChange={(value: string) => setSearchType(value)}
          >
            <Select.Option value="feeds">Новости</Select.Option>
            <Select.Option value="events">Мероприятия</Select.Option>
          </Select>
          <Input
            style={{ width: 240 }}
            prefix={<SearchOutlined />}
            suffix={
              <CloseOutlined
                onClick={() => {
                  setIsVisible(false);
                }}
              />
            }
            placeholder="Поиск..."
          />
        </Space>
      )}
    </>
  );
};

export default Search;
