import "./index.css";
import { Dropdown, Menu } from "antd";
import { FC } from "react";
import { ItemTypes } from "../../App";
import { constCategories } from "../../itemTypes";
import { DownOutlined, UserOutlined } from "@ant-design/icons";

enum MenuKeys {
  Feeds = "0",
  Events = "1",
  Games = "2",
  Cinema = "3",
  Education = "4",
  Entertaiment = "5",
  Sport = "6",
  Other = "7",
}

type SideMenuProps = {
  menuKey: ItemTypes;
  setMenuKey: React.Dispatch<React.SetStateAction<ItemTypes>>;
};

const SideMenu: FC<SideMenuProps> = ({ menuKey, setMenuKey }) => {
  let keys = 2;

  const categories = (
    <Menu>
      {constCategories.map((cat) => (
        <Menu.Item
          key={keys}
          onClick={() => {
            setMenuKey("events");
          }}
        >
          {cat}
        </Menu.Item>
      ))}
    </Menu>
  );

  return (
    <div className="menu">
      <Menu
        className="menu"
        mode="vertical"
        defaultSelectedKeys={[MenuKeys.Feeds]}
      >
        <Menu.Item
          key={MenuKeys.Feeds}
          onClick={() => {
            setMenuKey("feeds");
          }}
        >
          Новости
        </Menu.Item>
        <Dropdown overlay={categories}>
          <Menu.Item
            key={MenuKeys.Events}
            onClick={() => {
              setMenuKey("events");
            }}
          >
            Мероприятия <DownOutlined />
          </Menu.Item>
        </Dropdown>
      </Menu>
    </div>
  );
};

export default SideMenu;
