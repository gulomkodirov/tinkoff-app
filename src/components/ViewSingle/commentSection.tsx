import { Avatar, Divider, Input, Popconfirm, Space } from "antd";
import { FC, useEffect, useState } from "react";
import {
  UserOutlined,
  SendOutlined,
  DeleteOutlined,
  EditOutlined,
  CloseOutlined,
} from "@ant-design/icons";
import {
  createEventComment,
  createFeedComment,
  deleteEventComment,
  deleteFeedComment,
  editEventComment,
  editFeedComment,
  getEventComments,
  getFeedComments,
} from "../../api";
import { CommentType, constComments, declOfNums } from "../../itemTypes";
import { UserFullInfo } from "../../App";

type SingleType = "feed" | "event";

type CommentSectionProps = {
  user: UserFullInfo | null;
  singleId: number;
  singleType: SingleType;
};

const CommentSection: FC<CommentSectionProps> = ({
  user,
  singleId,
  singleType,
}) => {
  const [inputCreate, setInputCreate] = useState("");

  const [idEdit, setIdEdit] = useState<number | null>();
  const [inputEdit, setInputEdit] = useState("");

  const [comments, setComments] = useState<CommentType[]>(constComments);

  useEffect(() => {
    if (singleType === "feed") {
      getFeedComments(singleId);
    }

    if (singleType === "event") {
      getEventComments(singleId);
    }
  }, [singleId, singleType]);

  function sendComment(): void {
    if (!user) {
      return;
    }
    if (singleType === "feed") {
      createFeedComment(user.id, singleId, inputCreate);
    }

    if (singleType === "event") {
      createEventComment(user.id, singleId, inputCreate);
    }
  }

  function deleteComment(commentId: number): void {
    if (singleType === "feed") {
      deleteFeedComment(commentId);
    }

    if (singleType === "event") {
      deleteEventComment(commentId);
    }
  }

  function updateComment(id: number, text: string) {
    if (singleType === "feed") {
      editFeedComment(id, text);
    }

    if (singleType === "event") {
      editEventComment(id, text);
    }
  }

  return (
    <Space direction="vertical" style={{ width: "100%" }}>
      <Divider />
      {comments.length > 0 && (
        <h4>
          {comments.length} комментар
          {declOfNums(comments.length, ["ий", "ия", "иев"])}
        </h4>
      )}
      {user && (
        <>
          <Space
            direction="horizontal"
            style={{ width: "100%", paddingBottom: "40px" }}
            align="center"
          >
            {user.url ? (
              <Avatar shape="circle" size="large" src={user.url} />
            ) : (
              <Avatar shape="circle" size="large" icon={<UserOutlined />} />
            )}
            <Input
              style={{ width: "40vw" }}
              placeholder="Оставьте комментарий..."
              bordered={true}
              value={inputCreate}
              onChange={(e) => {
                setInputCreate(e.target.value);
              }}
              onPressEnter={sendComment}
            />
            <SendOutlined style={{ fontSize: "25px" }} onClick={sendComment} />
          </Space>
        </>
      )}
      {comments.length > 0 &&
        comments.map((comm) => (
          <Space
            direction="horizontal"
            style={{
              width: "100%",
              display: "grid",
              gridTemplateColumns: "2fr 1fr 7fr 1fr",
            }}
            align="center"
          >
            <h4>{comm.author.name}</h4>
            <Divider type="vertical" />
            {comm.id !== idEdit && <h4>{comm.text}</h4>}
            {comm.id === idEdit && (
              <Input
                style={{ width: "100%" }}
                bordered={false}
                placeholder={comm.text}
                value={inputEdit}
                onChange={(e) => {
                  setInputEdit(e.target.value);
                }}
                suffix={
                  <CloseOutlined
                    onClick={() => {
                      setIdEdit(null);
                    }}
                  />
                }
                onPressEnter={() => {
                  updateComment(comm.id, inputEdit);
                  setIdEdit(null);
                }}
              />
            )}
            {user && comm.author.id === user.id && (
              <Space>
                {comm.id !== idEdit && (
                  <EditOutlined
                    onClick={() => {
                      setInputEdit(comm.text);
                      setIdEdit(comm.id);
                    }}
                  />
                )}
                <Popconfirm
                  title="Уверены, что хотите удалить комментарий?"
                  onConfirm={() => {
                    deleteComment(comm.id);
                  }}
                  okText="Удалить"
                  cancelText="Отмена"
                >
                  <DeleteOutlined />
                </Popconfirm>
              </Space>
            )}
          </Space>
        ))}
    </Space>
  );
};

export default CommentSection;
