import { Avatar, Badge, Divider, Modal, Space, Typography } from "antd";
import { FC } from "react";
import { EventType, FeedType, getDateEvent } from "../../itemTypes";
import {
  ClockCircleOutlined,
  CompassOutlined,
  GlobalOutlined,
  UserOutlined,
} from "@ant-design/icons";
import CommentSection from "./commentSection";
import { UserFullInfo } from "../../App";
const { Link } = Typography;

type ViewFeedProps = {
  user: UserFullInfo | null;
  visible: boolean;
  setVisible: React.Dispatch<React.SetStateAction<boolean>>;
  type: "feed" | "event";
  single: FeedType | EventType;
};

const ViewSingle: FC<ViewFeedProps> = ({
  user,
  visible,
  setVisible,
  type,
  single,
}) => {
  return (
    <Modal
      width="50vw"
      visible={visible}
      footer={null}
      onCancel={() => {
        setVisible(false);
      }}
    >
      <Space style={{ paddingBottom: "20px" }}>
        <Badge>
          {single.author.url ? (
            <Avatar shape="square" size={60} src={single.author.url} />
          ) : (
            <Avatar shape="square" size={60} icon={<UserOutlined />} />
          )}
        </Badge>
        <Space direction="vertical" size={0}>
          <h3 style={{ marginBottom: 0 }}>{single.author.name}</h3>
          <h5 style={{ marginBottom: 0 }}>
            {type === "feed"
              ? getDateEvent((single as FeedType).publicationDate)
              : getDateEvent((single as EventType).creationTime)}
          </h5>
        </Space>
      </Space>

      <h1 style={{ width: "100%" }}>
        {type === "feed"
          ? (single as FeedType).title
          : (single as EventType).name}
      </h1>
      <Space
        direction="vertical"
        style={{ width: "100%", paddingBottom: "30px" }}
      >
        {(single as EventType).address && (
          <Space>
            <CompassOutlined style={{ fontSize: "20px" }} />
            <h4 style={{ marginBottom: 0 }}>{(single as EventType).address}</h4>
          </Space>
        )}
        {(single as EventType).link && (
          <Space align="center">
            <GlobalOutlined style={{ fontSize: "20px" }} />
            <Link style={{ marginBottom: 0 }}>
              {(single as EventType).link}
            </Link>
          </Space>
        )}
        {(single as EventType).eventTime && (
          <>
            <Space align="center">
              <ClockCircleOutlined style={{ fontSize: "20px" }} />
              <h4 style={{ marginBottom: 0 }}>
                {getDateEvent((single as EventType).eventTime)}
              </h4>
            </Space>
            <Divider />
          </>
        )}
      </Space>
      <Space align="start">
        <h3>{single.description}</h3>
      </Space>
      <CommentSection user={user} singleId={single.id} singleType={type} />
    </Modal>
  );
};

export default ViewSingle;
